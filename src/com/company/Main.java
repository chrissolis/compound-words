package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Verifier checker = new Verifier();
        ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("alien");
        list.add("born");
        list.add("less");
        list.add("lien");
        list.add("nevertheless");
        list.add("new");
        list.add("newborn");
        list.add("the");
        list.add("zebra");

        //checker.checkWordByLetter("throughout");
        checker.compoundListSpellChecker(list);

        checker.printResultList();
    }
}
