package com.company;

import java.util.ArrayList;

/**
 * Created by christopher_solis on 1/31/19.
 */
public class Verifier {
    private WordContainer wordMap;
    private ArrayList<String> resultList;

    public Verifier() {
        this.wordMap = new WordContainer();
        this.resultList  = new ArrayList<String>();
    }

    public WordContainer getWordMap() {
        return wordMap;
    }

    public void setWordMap(WordContainer wordMap) {
        this.wordMap = wordMap;
    }

    public ArrayList<String> getResultList() {
        return resultList;
    }

    public void setResultList(ArrayList<String> resultList) {
        this.resultList = resultList;
    }

    public void compoundListSpellChecker(ArrayList<String> newWords){
        for (int i = 0; i < newWords.size(); i++){
            checkWordByLetter(newWords.get(i));
        }
    }

    public boolean searchRealWords(String wordInQuestion){
        //Compares given word with words that are in the hashmap
        //if word is real, then this will return true.
        boolean isReal = getWordMap().getWordContainer().containsKey(wordInQuestion);
        return isReal;
    }

    public void checkWordByLetter(String wordInQuestion){
        String tempWord = "";
        String subWord1 = "";
        String subWord2 = "";
        int wordCount = 0;

        if(wordInQuestion.isEmpty()){
            return;
        }

        for(int i = 0; i < wordInQuestion.length(); i++){
            tempWord += wordInQuestion.charAt(i);
            if (searchRealWords(tempWord)){
                if (wordCount == 0){
                    subWord1 = tempWord;
                }
                if (wordCount == 1){
                    subWord2 = tempWord;
                }
                tempWord = "";
                wordCount++;
            }
        }

        if (wordCount == 2){
            getResultList().add(wordInQuestion);
            //addCompoundToResult(subWord1, subWord2);
        }
    }

    public void addCompoundToResult(String word1, String word2){
        getResultList().add(word1);
        getResultList().add(word2);
    }

    public boolean singleLetterWord(String newWords, int index){
        if(newWords.length() < 2){
            return true;
        }
        else{
            return false;
        }
    }

    public void printResultList(){
        for (int i = 0; i < getResultList().size(); i++){
            System.out.println(getResultList().get(i));
        }
    }
}
