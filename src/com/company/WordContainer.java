package com.company;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by christopher_solis on 1/29/19.
 */
public class WordContainer {
    private HashMap<String, String> wordContainer = new HashMap<String, String>();
    private String fileName;


    public WordContainer() {
        setFileName("words.txt");
        setWordContainer(readFile(getFileName()));
    }

    public HashMap<String, String> getWordContainer() {
        return wordContainer;
    }

    public void setWordContainer(HashMap<String, String> wordContainer) {
        this.wordContainer = wordContainer;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public HashMap<String, String> readFile(String filename){
        int i = 0;
        try {
            Scanner in = new Scanner(new File(filename));
            String line;
            while(in.hasNext())
            {
                line = in.nextLine();
                wordContainer.put(line, line);
                i++;
            }
            in.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        return wordContainer;
    }

    public void printWords(){
        for (String word : wordContainer.keySet()){
            String value = wordContainer.get(word);
            System.out.println(value);

        }
    }
}


